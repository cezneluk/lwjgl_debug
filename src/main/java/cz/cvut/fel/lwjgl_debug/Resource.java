package cz.cvut.fel.lwjgl_debug;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that offer a loader resources from java resources
 */
public class Resource {
	public static final Logger LOGGER = Logger.getLogger(Resource.class.getName());

	URL path;
	byte[] data;

	/**
	 * Create new resource from path
	 *
	 * @param path path to java resource, not absolute
	 */
	public Resource(String path) throws IllegalArgumentException {
		this.path = getClass().getClassLoader().getResource(path);
		if (this.path == null)
			throw new IllegalArgumentException("Path (" + path + ") is not valid resource");
		data = null;
	}

	/**
	 * Create new resource from memory data
	 */
	public Resource(byte[] data) {
		path = null;
		this.data = data;
	}

	/**
	 * @return path of resource or null if this resource is in memory
	 */
	public String getPath() {
		return path != null ? path.getPath() : null;
	}

	/**
	 * @return true is this resource is in memory
	 */
	public boolean isInMemory() {
		return data != null;
	}

	/**
	 * @return stream of this resource data
	 */
	public InputStream getStream() {
		if (data != null)
			return new ByteArrayInputStream(data);
		try {
			return path.openStream();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Unexpected IOException - path resource was checked in constructor", e);
			return null;
		}
	}

	/**
	 * @return get resource as text (read whole file)
	 */
	public String getText() {
		Scanner scanner = new Scanner(getStream()).useDelimiter("\\A");
		String output = scanner.next();
		scanner.close();
		return output;
	}

	/**
	 * @return get resource as buffer
	 */
	public ByteBuffer getBuffer() {
		if (data != null)
			return ByteBuffer.wrap(data);
		try (InputStream stream = getStream()) {
			return ByteBuffer.wrap(stream.readAllBytes());
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Unexpected IOException - path resource was checked in constructor", e);
			return null;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Resource resource = (Resource) o;

		if (path != null)
			return resource.path != null && path.equals(resource.path);
		if (data != null)
			return resource.data != null && Arrays.equals(data, resource.data);
		return false;
	}

	@Override
	public int hashCode() {
		if (path != null)
			return path.hashCode();
		if (data != null)
			return Arrays.hashCode(data);
		return 0;
	}

	@Override
	public String toString() {
		return "[Resource] " + (data != null ? "Raw data of size " + data.length : path);
	}
}