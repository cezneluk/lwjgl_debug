package cz.cvut.fel.lwjgl_debug;

/**
 * Represent color mode of a texture
 */
public enum ColorMode {
	/**
	 * Grayscale texture (only red channel in shader)
	 */
	R(1),
	/**
	 * Texture with 2 color components
	 */
	RG(2),
	/**
	 * Solid RGB texture without alpha
	 */
	RGB(3),
	/**
	 * Full RGBA texture
	 */
	RGBA(4);

	int componentsCount;

	ColorMode(int componentsCount) {
		this.componentsCount = componentsCount;
	}

	public int getComponentsCount() {
		return componentsCount;
	}

	/**
	 * @param componentsCount number of texture components
	 * @return Proper color mode from {@param componentsCount}
	 */
	public static ColorMode fromComponents(int componentsCount) {
		switch (componentsCount) {
			case 1:
				return ColorMode.R;
			case 2:
				return ColorMode.RG;
			case 3:
				return ColorMode.RGB;
			case 4:
				return ColorMode.RGBA;
			default:
				throw new IllegalArgumentException("Invalid componentsCount");
		}
	}
}
