package cz.cvut.fel.lwjgl_debug;

import org.lwjgl.util.tinyfd.TinyFileDialogs;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Starter {
	public static void main(String[] args) {
			Main main = new Main();
			main.start();
	}
}
