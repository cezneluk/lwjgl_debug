package cz.cvut.fel.lwjgl_debug;

import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glDeleteShader;

public class GLShaderCompiler {
	private final static Logger LOGGER = Logger.getLogger(GLShaderCompiler.class.getName());

	int compiledVertex = -1;
	int compiledFragment = -1;

	Resource vertexShader;
	Resource fragmentShader;

	public GLShaderCompiler(Resource vertexShader, Resource fragmentShader) {
		this.vertexShader = vertexShader;
		this.fragmentShader = fragmentShader;
	}

	private int compileShader(int type, Resource resource) {
		int shader = glCreateShader(type);
		glShaderSource(shader, resource.getText());
		Debugger.checkError("glShaderSource(" + shader + ", " + resource.getText() + ")");
		glCompileShader(shader);
		Debugger.checkError("glCompileShader(" + shader + ")");

		try (MemoryStack stack = MemoryStack.stackPush()) {
			IntBuffer success = stack.mallocInt(1);
			glGetShaderiv(shader, GL_COMPILE_STATUS, success);

			if (success.get(0) == 0) {
				LOGGER.log(Level.SEVERE, "Unable to compile shader: " + glGetShaderInfoLog(shader));
				return -1;
			}
		}

		return shader;
	}

	public void compile() {
		compiledVertex = compileShader(GL_VERTEX_SHADER, vertexShader);
		compiledFragment = compileShader(GL_FRAGMENT_SHADER, fragmentShader);
	}

	public GLShader createInstance() {
		int shaderProgram = glCreateProgram();
		Debugger.checkError("glCreateProgram()");
		glAttachShader(shaderProgram, compiledVertex);
		Debugger.checkError("glAttachShader("+shaderProgram+", "+compiledVertex+")");
		glAttachShader(shaderProgram, compiledFragment);
		Debugger.checkError("glAttachShader("+shaderProgram+", "+compiledFragment+")");
		glLinkProgram(shaderProgram);
		Debugger.checkError("glLinkProgram("+shaderProgram+")");

		return new GLShader(shaderProgram);
	}

	public void dispose() {
		if (compiledVertex >= 0) {
			glDeleteShader(compiledVertex);
			Debugger.checkError("glDeleteShader("+compiledVertex+")");
		}
		if (compiledFragment >= 0) {
			glDeleteShader(compiledFragment);
			Debugger.checkError("glDeleteShader("+compiledFragment+")");
		}
	}
}