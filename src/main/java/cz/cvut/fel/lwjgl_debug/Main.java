package cz.cvut.fel.lwjgl_debug;

import org.joml.Matrix4f;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.Callback;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL30.*;

public class Main {
	long windowId;

	/**
	 * Name of outline color uniform name in glsl shader
	 */
	public static final String OUTLINE_COLOR_NAME = "outline_color";
	/**
	 * Name of outline width uniform name in glsl shader
	 */
	public static final String OUTLINE_WIDTH_NAME = "outline_width";
	/**
	 * Name of outline color uniform name in glsl shader
	 */
	public static final String TEXTURE_NAME = "sprite";

	public static final Vector4f DEFAULT_OUTLINE_COLOR = new Vector4f(1f, 1f, 1f, 1f);
	public static final float DEFAULT_OUTLINE_WIDTH = 0.05f;

	protected static final float TRIANGLE_HEIGHT = (float) (Math.sqrt(3) / 4);

	private static final float[] VERTEX_DATA = {
			//position, uv, outline_index
			0f, 0f, 0.5f, 0.5f, 0f,
			-0.25f, -TRIANGLE_HEIGHT, 0.25f, 0.5f + TRIANGLE_HEIGHT, 1f,
			0.25f, -TRIANGLE_HEIGHT, 0.75f, 0.5f + TRIANGLE_HEIGHT, 1f,
			0.5f, 0f, 1f, 0.5f, 1f,
			0.25f, TRIANGLE_HEIGHT, 0.75f, 0.5f - TRIANGLE_HEIGHT, 1f,
			-0.25f, TRIANGLE_HEIGHT, 0.25f, 0.5f - TRIANGLE_HEIGHT, 1f,
			-0.5f, 0f, 0f, 0.5f, 1f
	};

	private static final int[] TRIANGLE_DATA = {
			0, 1, 2,
			0, 2, 3,
			0, 3, 4,
			0, 4, 5,
			0, 5, 6,
			0, 6, 1
	};

	private static final GLVertexMapping MAPPING = new GLVertexMapping(new GLVertexMapping.MappingElement[]{
			new GLVertexMapping.MappingElement(0, 2, 0),
			new GLVertexMapping.MappingElement(1, 2, 2 * 4),
			new GLVertexMapping.MappingElement(2, 1, 4 * 4)
	});

	int objectId;
	int vertexBuffer;
	int triangleBuffer;
	int triangleCount;
	GLShader shader;

	public Main() {
		if (!glfwInit()) {
			printError("glfwInit", "Unable to initialize GLFW!");
		}

		windowId = glfwCreateWindow(800, 600, "lwjgl debug", 0, 0);
		if (windowId == 0) {
			printError("glfwCreateWindow", "Unable to create a window");
		}

		glfwMakeContextCurrent(windowId);

		glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_FALSE);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
		GL.createCapabilities();
		Callback debugProc = GLUtil.setupDebugMessageCallback();

		Debugger.printGLInfo();
	}

	private void printError(String function, String error) {
		System.out.println(function + ": " + error);
		System.exit(1);
	}

	public void start() {
		initObject(VERTEX_DATA,MAPPING,TRIANGLE_DATA);

		for(int i = 0; i < 5; i++) {
		/*int i = -1;
		while(!glfwWindowShouldClose(windowId)) {
			i++;*/
			preRender();
			System.out.println("[PRE_RENDER "+i+"]");
			render();
			System.out.println("[RENDER "+i+"]");
			postRender();
			System.out.println("[POST_RENDER "+i+"]");
		}

		System.out.println("[DISPOSE]");
		dispose();
	}

	void initObject(float[] vertexData, GLVertexMapping mapping, int[] triangleData) {
		objectId = glGenVertexArrays();
		Debugger.checkError("glGenVertexArrays()");

		glBindVertexArray(objectId);
		Debugger.checkError("glBindVertexArray("+objectId+")");

		vertexBuffer = glGenBuffers();
		Debugger.checkError("glGenBuffers()");
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		Debugger.checkError("glBindBuffer(GL_ARRAY_BUFFER, "+vertexBuffer+")");
		glBufferData(GL_ARRAY_BUFFER, vertexData, GL_STATIC_DRAW);
		Debugger.checkError("glBufferData(GL_ARRAY_BUFFER, "+ Arrays.toString(vertexData) +", GL_STATIC_DRAW)");

		triangleBuffer = glGenBuffers();
		Debugger.checkError("glGenBuffers()");
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleBuffer);
		Debugger.checkError("glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, "+triangleBuffer+")");
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangleData, GL_STATIC_DRAW);
		Debugger.checkError("glBufferData(GL_ELEMENT_ARRAY_BUFFER, "+ Arrays.toString(triangleData) +", GL_STATIC_DRAW)");

		triangleCount = triangleData.length;

		// Set vertex data mapping
		mapping.setVertexAttribs();

		// Unbind resources
		glBindVertexArray(0);
		Debugger.checkError("glBindVertexArray(0)");

		//Texture
		GLTexture texture = new GLTexture(new Resource("mountain.png"));

		//Shader
		GLShaderCompiler shaderCompiler = new GLShaderCompiler(new Resource("tile.vert"), new Resource("tile.frag"));
		shaderCompiler.compile();
		shader = shaderCompiler.createInstance();
		shaderCompiler.dispose();

		shader.insertVector4(OUTLINE_COLOR_NAME,DEFAULT_OUTLINE_COLOR);
		shader.insertScalar(OUTLINE_WIDTH_NAME,DEFAULT_OUTLINE_WIDTH);
		shader.insertTexture(TEXTURE_NAME,texture);
	}

	void preRender() {
		glfwMakeContextCurrent(windowId);
		glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		Debugger.checkError("glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT)");
		glEnable(GL_BLEND);
		Debugger.checkError("glEnable(GL_BLEND)");
		glDisable(GL_DEPTH_TEST);
		Debugger.checkError("glDisable(GL_DEPTH_TEST)");

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		Debugger.checkError("glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)");

		try (MemoryStack stack = MemoryStack.stackPush()) {
			FloatBuffer loc2world = new Matrix4f().get(stack.mallocFloat(16));
			FloatBuffer world2clip = new Matrix4f().ortho2D(-1, 1, -1, 1).get(stack.mallocFloat(16));

			FloatBuffer time = stack.floats((float) glfwGetTime());

			shader.setDefaultValues(loc2world,world2clip, time);
		}
	}

	void render() {
		if (objectId < 0)
			throw new RuntimeException("Calling render before init");

		glBindVertexArray(objectId);
		Debugger.checkError("glBindVertexArray("+objectId+")");
		shader.use();

		glDrawElements(GL_TRIANGLES, triangleCount, GL_UNSIGNED_INT, 0);
		Debugger.checkError("glDrawElements(GL_TRIANGLES, "+triangleCount+", GL_UNSIGNED_INT, 0)");

		shader.unUse();
		glBindVertexArray(0);
		Debugger.checkError("glBindVertexArray(0)");
	}

	void postRender() {
		glfwPollEvents();
		glfwSwapBuffers(windowId);
	}

	void dispose() {
		disposeObject();
		shader.dispose();
		glfwDestroyWindow(windowId);
	}

	void disposeObject() {
		glDeleteBuffers(triangleBuffer);
		Debugger.checkError("glDeleteBuffers("+triangleBuffer+")");
		glDeleteBuffers(vertexBuffer);
		Debugger.checkError("glDeleteBuffers("+vertexBuffer+")");
		glDeleteVertexArrays(objectId);
		Debugger.checkError("glDeleteVertexArrays("+objectId+")");
	}
}
