package cz.cvut.fel.lwjgl_debug;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL43;

public class Debugger {
	public static void checkError(String command) {
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();

		int errorCode = GL11.glGetError();
		if(errorCode == GL11.GL_NO_ERROR)
			System.out.println("[SUCCESS] " + command + "\n\t" + stack[2]);
		else
			System.out.println("[ERROR] " + command + ": " + String.format("0x%08X", errorCode) + "\n\t" + stack[2]);
	}

	public static void printGLInfo() {
		System.out.println("GL_VERSION: " + GL11.glGetString(GL11.GL_VERSION));
		System.out.println("GL_VENDOR: " + GL11.glGetString(GL11.GL_VENDOR));
		System.out.println("GL_RENDERER: " + GL11.glGetString(GL11.GL_RENDERER));

		int extensionsLength = GL11.glGetInteger(GL30.GL_NUM_EXTENSIONS);
		System.out.println("GL_EXTENSIONS: " + extensionsLength);
		for(int i = 0; i < extensionsLength; i++) {
			System.out.println("\t" + GL30.glGetStringi(GL11.GL_EXTENSIONS, i));
		}

		System.out.println("GL_SHADING_LANGUAGE_VERSION: " + GL11.glGetString(GL30.GL_SHADING_LANGUAGE_VERSION));

		int versionsLength = GL11.glGetInteger(GL43.GL_NUM_SHADING_LANGUAGE_VERSIONS);
		System.out.println("Supported versions: " + versionsLength);
		for(int i = 0; i < versionsLength; i++) {
			System.out.println("\t" + GL30.glGetStringi(GL30.GL_SHADING_LANGUAGE_VERSION,i));
		}
	}
}
