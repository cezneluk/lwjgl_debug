package cz.cvut.fel.lwjgl_debug;

import org.lwjgl.system.MemoryStack;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL42.glTexStorage2D;
import static org.lwjgl.stb.STBImage.stbi_failure_reason;
import static org.lwjgl.stb.STBImage.stbi_load_from_memory;

public class GLTexture {
	public static final Logger LOGGER = Logger.getLogger(GLTexture.class.getName());

	int textureId;

	int width;
	int height;
	ColorMode colorMode;

	ByteBuffer rawData;

	Resource resource;

	public GLTexture(Resource resource) {
		this.resource = resource;
		load();
	}

	private static int mapColorMode(ColorMode mode, int glR, int glRg, int glRgb, int glRgba) {
		switch (mode) {
			case R:
				return glR;
			case RG:
				return glRg;
			case RGB:
				return glRgb;
			case RGBA:
				return glRgba;
			default:
				return -1;
		}
	}

	public static int glInternalFormatFromColorMode(ColorMode mode) {
		return mapColorMode(mode, GL_R8, GL_RG8, GL_RGB8, GL_RGBA8);
	}

	public static int glFormatFromColorMode(ColorMode mode) {
		return mapColorMode(mode, GL_RED, GL_RG, GL_RGB, GL_RGBA);
	}

	/**
	 * Load texture via stbi and set proper GL parameters
	 */
	
	protected void load() {
		ByteBuffer managedBuffer = resource.getBuffer();
		try (MemoryStack stack = MemoryStack.stackPush()) {
			ByteBuffer compressedBuffer = stack.malloc(managedBuffer.capacity());
			compressedBuffer.put(managedBuffer);
			compressedBuffer.flip();

			IntBuffer widthBuffer = stack.mallocInt(1);
			IntBuffer heightBuffer = stack.mallocInt(1);
			IntBuffer componentsBuffer = stack.mallocInt(1);

			rawData = stbi_load_from_memory(compressedBuffer, widthBuffer, heightBuffer, componentsBuffer, 0);
			if (rawData == null) {
				LOGGER.log(Level.SEVERE, "Image decompression failed: " + stbi_failure_reason());
				width = 0;
				height = 0;
				colorMode = null;
				textureId = 0;
				return;
			}

			width = widthBuffer.get(0);
			height = heightBuffer.get(0);
			colorMode = ColorMode.fromComponents(componentsBuffer.get(0));

			textureId = glGenTextures();
			Debugger.checkError("glGenTextures()");
			glBindTexture(GL_TEXTURE_2D, textureId);
			Debugger.checkError("glBindTexture(GL_TEXTURE_2D, "+textureId+")");

			glTexStorage2D(GL_TEXTURE_2D, 1, glInternalFormatFromColorMode(colorMode), width, height);
			Debugger.checkError("glTexStorage2D(GL_TEXTURE_2D, 1, "+glInternalFormatFromColorMode(colorMode)+", "+width+", "+height+")");
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, glFormatFromColorMode(colorMode), GL_UNSIGNED_BYTE, rawData);
			Debugger.checkError("glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, "+width+", "+height+", "+glFormatFromColorMode(colorMode)+", GL_UNSIGNED_BYTE, rawData)");

			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			Debugger.checkError("glPixelStorei(GL_UNPACK_ALIGNMENT, 1)");
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			Debugger.checkError("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)");
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			Debugger.checkError("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)");
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			Debugger.checkError("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)");
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			Debugger.checkError("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)");
		}
	}

	
	public int getWidth() {
		return width;
	}

	
	public int getHeight() {
		return height;
	}

	
	public ColorMode getColorMode() {
		return colorMode;
	}

	
	public void dispose() {
		if (textureId >= 0) {
			glDeleteTextures(textureId);
			Debugger.checkError("glDeleteTextures(textureId)");
			width = 0;
			height = 0;
			colorMode = null;
		}
	}

	/**
	 * @return GL unique identifier
	 */
	public int getTextureId() {
		return textureId;
	}

	public ByteBuffer getRawData() {
		return rawData;
	}

	
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		GLTexture texture = (GLTexture) o;

		return textureId == texture.textureId;
	}

	
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + textureId;
		return result;
	}
}