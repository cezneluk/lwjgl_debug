package cz.cvut.fel.lwjgl_debug;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;

/**
 * Helper class for better defining of vertex mapping
 */
public final class GLVertexMapping {
	public static final class MappingElement {
		final int index;
		final int size;
		final int start;

		public MappingElement(int index, int size, int start) {
			this.index = index;
			this.size = size;
			this.start = start;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			MappingElement element = (MappingElement) o;

			if (index != element.index) return false;
			if (size != element.size) return false;
			return start == element.start;
		}

		@Override
		public int hashCode() {
			int result = index;
			result = 31 * result + size;
			result = 31 * result + start;
			return result;
		}
	}

	int stride;
	List<MappingElement> elements;

	public GLVertexMapping() {
		stride = 0;
		elements = new ArrayList<>();
	}

	public GLVertexMapping(MappingElement[] elements) {
		this.elements = new ArrayList<MappingElement>(elements.length);
		for (MappingElement element : elements) {
			this.elements.add(element);
			stride += element.size * 4;
		}
	}

	public int getStride() {
		return stride;
	}

	public void addElement(int index, int size) {
		MappingElement element = new MappingElement(index, size, stride);
		elements.add(element);
		stride += size * 4;
	}

	/**
	 * Set proper vertex attributes to current selected vertex buffer
	 */
	public void setVertexAttribs() {
		for (MappingElement element : elements) {
			glVertexAttribPointer(element.index, element.size, GL_FLOAT, false, stride, element.start);
			Debugger.checkError("glVertexAttribPointer("+element.index+", "+element.size+", GL_FLOAT, false, "+stride+", "+element.start+")");
			glEnableVertexAttribArray(element.index);
			Debugger.checkError("glEnableVertexAttribArray("+element.index+")");
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		GLVertexMapping that = (GLVertexMapping) o;

		if (stride != that.stride) return false;
		return elements.equals(that.elements);
	}

	@Override
	public int hashCode() {
		int result = stride;
		result = 31 * result + elements.hashCode();
		return result;
	}
}