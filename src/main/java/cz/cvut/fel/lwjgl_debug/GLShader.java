package cz.cvut.fel.lwjgl_debug;

import org.joml.Matrix4fc;
import org.joml.Vector2fc;
import org.joml.Vector3fc;
import org.joml.Vector4fc;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL41.*;

public class GLShader {
	private final static Logger LOGGER = Logger.getLogger(GLShader.class.getName());

	public final static int LOC2WORLD_LOCATION = 0;
	public final static int WORLD2CLIP_LOCATION = 1;
	public final static int TIME_LOCATION = 2;

	int shaderId;
	Map<Integer, GLTexture> textures;

	public GLShader(int shaderId) {
		this.shaderId = shaderId;
		textures = new HashMap<>();
	}

	/**
	 * Set this shader as active
	 */
	public void use() {
		glUseProgram(shaderId);
		Debugger.checkError("glUseProgram(" + shaderId + ")");

		int textureIndex = 0;
		for (Map.Entry<Integer, GLTexture> texture : textures.entrySet()) {
			glUniform1i(texture.getKey(), textureIndex);
			Debugger.checkError("glUniform1i(" + texture.getKey() + ", " + textureIndex + ")");
			glActiveTexture(GL_TEXTURE0 + textureIndex);
			Debugger.checkError("glActiveTexture(GL_TEXTURE0 + " + textureIndex + ")");
			textureIndex++;
			glBindTexture(GL_TEXTURE_2D, texture.getValue().getTextureId());
			Debugger.checkError("glBindTexture(GL_TEXTURE_2D, " + texture.getValue().getTextureId() + ")");
		}
	}

	/**
	 * Reset active shader
	 */
	public void unUse() {
		glUseProgram(0);
		Debugger.checkError("glUseProgram(0)");
	}
	
	public void insertVector4(String name, Vector4fc vector) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			FloatBuffer vectorBuffer = vector.get(stack.mallocFloat(4));

			int location = glGetUniformLocation(shaderId, name);
			Debugger.checkError("glGetUniformLocation(" + shaderId + ", " + name + ")");
			if (location < 0)
				LOGGER.log(Level.WARNING, "Cannot set Vector4fc with name" + name);
			else {
				glProgramUniform4fv(shaderId, location, vectorBuffer);
				Debugger.checkError("glProgramUniform4fv(" + shaderId + ", " + location + ", " + vectorBuffer + ")");
			}
		}
	}

	
	public void insertScalar(String name, float scalar) {
		int location = glGetUniformLocation(shaderId, name);
		Debugger.checkError("glGetUniformLocation(" + shaderId + ", " + name + ")");
		if (location < 0)
			LOGGER.log(Level.WARNING, "Cannot set float with name " + name);
		else {
			glProgramUniform1f(shaderId, location, scalar);
			Debugger.checkError("glProgramUniform1f(" + shaderId + ", " + location + ", " + scalar + ")");
		}
	}

	
	public void insertTexture(String name, GLTexture texture) {
			int location = glGetUniformLocation(shaderId, name);
		Debugger.checkError("glGetUniformLocation(" + shaderId + ", " + name + ")");
			if (location < 0)
				LOGGER.log(Level.WARNING, "Cannot set texture with name " + name);
			else
				textures.put(location, (GLTexture) texture);
	}

	public void setDefaultValues(FloatBuffer loc2world, FloatBuffer world2clip, FloatBuffer time) {
		glProgramUniformMatrix4fv(shaderId, LOC2WORLD_LOCATION, false, loc2world);
		Debugger.checkError("glProgramUniformMatrix4fv(" + shaderId + ", " + LOC2WORLD_LOCATION + ", false, " + loc2world + ")");
		glProgramUniformMatrix4fv(shaderId, WORLD2CLIP_LOCATION, false, world2clip);
		Debugger.checkError("glProgramUniformMatrix4fv(" + shaderId + ", " + WORLD2CLIP_LOCATION + ", false, " + world2clip + ")");
		glProgramUniform1fv(shaderId, TIME_LOCATION, time);
		Debugger.checkError("glProgramUniform1fv(" + shaderId + ", " + TIME_LOCATION + ", " + time + ")");
	}

	
	public void dispose() {
		glDeleteProgram(shaderId);
		Debugger.checkError("glDeleteProgram(" + shaderId + ")");
	}

	
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		GLShader glShader = (GLShader) o;

		return shaderId == glShader.shaderId;
	}

	
	public int hashCode() {
		return shaderId;
	}

	
	public String toString() {
		return "[GLShader " + shaderId + "]";
	}
}
