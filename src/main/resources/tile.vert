#version 430 core
layout (location = 0) in vec2 vert_position;
layout (location = 1) in vec2 vert_texCoord;
layout (location = 2) in float vert_outline;

layout (location = 0) uniform mat4 jtbse_loc2world;
layout (location = 1) uniform mat4 jtbse_world2clip;
layout (location = 2) uniform float jtbse_time;

out vec2 frag_texCoord;
out float frag_outline;

void main() {
    gl_Position = jtbse_world2clip * jtbse_loc2world * vec4(vert_position, 0.0, 1.0);
    frag_texCoord = vert_texCoord;
    frag_outline = vert_outline;
}
