#version 430 core
in vec2 frag_texCoord;
in float frag_outline;

uniform sampler2D sprite;
uniform vec4 outline_color;
uniform float outline_width;

void main() {
    float outline_a = outline_color.a * max(sign(frag_outline - 1 + outline_width), 0);

    vec4 color = texture(sprite, frag_texCoord) * (1 - outline_a) + vec4(outline_color.rgb, 1.0) * outline_a;
    gl_FragColor = color;
}
